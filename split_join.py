"""
Given a string, you need to reverse the order of characters in each word
within a sentence while still preserving whitespace and initial word order.
In the string, each word is separated by single space and there will not be
any extra space in the string.
"""


def main():
    sentence = input().split(" ")
    reverse = " ".join(i[::-1] for i in sentence)
    print(reverse)


if __name__ == "__main__":
    main()
