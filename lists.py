"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each
command in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands
described above.
"""


def main():
    """Perform list commands."""
    n = int(input())
    tasklist = []
    while n > 0:
        action = input().split(' ')
        if action[0] == 'insert':
            tasklist.insert(int(action[1]), int(action[2]))
        elif action[0] == 'print':
            print(tasklist)
        elif action[0] == 'remove':
            tasklist.remove(int(action[1]))
        elif action[0] == 'append':
            tasklist.append(int(action[1]))
        elif action[0] == 'sort':
            tasklist.sort()
        elif action[0] == 'pop':
            tasklist.pop()
        elif action[0] == 'reverse':
            tasklist = tasklist[::-1]

        n -= 1


if __name__ == "__main__":
    main()
