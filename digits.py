"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = input()
    numtuple = tuple(n)
    result = 0
    if int(n) >= 0:
        for i in numtuple:
            result += int(i)
        print(result)


if __name__ == "__main__":
    main()
