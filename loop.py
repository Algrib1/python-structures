"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input())
    result = 1
    while n > 1:
        result *= n
        n -= 1
    print(result)


if __name__ == "__main__":
    main()
