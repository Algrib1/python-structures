"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())
    task = {key: value for (key, value) in d.items() if value is not None}
    #print(json.dumps(task))
    print(task)

if __name__ == "__main__":
    main()
